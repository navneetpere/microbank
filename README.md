# Clara Webapp

## 1. Package manager
Please user yarn as a package manager.\
To install yarn please visit [https://yarnpkg.com/getting-started/install](https://yarnpkg.com/getting-started/install)
## 2. Available Scripts

In the project directory, you can run:
```
yarn start
```

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

```
yarn test
```

Launches the test runner in the interactive watch mode.

```
yarn build
```

Builds the app for production to the `build` folder.

## 3. Making changes for new features
To create a new feature for a specific user use the command
Please use `camelCase` for the < featureName >.
The `user` is the folder in /features where the `featureName` template will be created. \
A `featureName` is to be considered a new route for one of the three authorised users from `admin` | `brand` | `creator`.

<user> must either of the 3: admin, brand, creator

```
yarn newFeat <user> <featureName>
```
e.g.
```
yarn newFeat admin homePage
```

## 4. Create a shared component
USE SHARED COMPONENT ONLY FOR PURE STATELESS COMPONENTS

```
yarn shared <componentName>
```
e.g.
```
yarn shared profileComponent
```


Guidelines :

- File name should be in camelCase eg: serviceWorker.ts
- No special characters to be used in file name, we can use '.' only for test, style or config files eg: app.test.tsx, app.styles.tsx, jest.config.ts
- Do not use prefix or suffix like component or container
- Branch name should in following format
    feature/feature_name
    bug/bug_name
    test/testing_feature_name
- Function name, class name should be PascalCase eg: FunctionName, ClassName
- Block scope variable name should be camelCase, global case should CAPITALCASE eg const colorName, const COLOR_NAME
- Add comments for each functionality
- Naming: Variables & constants camelCase,short and clear name
- Variables for API request/response should reflect API request/responseMethods & functions, Classes & Modules use camelCase and verbs
- All components used and app level should be a part of /core
- Template components that do not hold data and are not connected to redux should be in the /shared