all:

qa:
	sed -i -e "s/claraapp-fe-env/claraapp-fe-qa/" docker-compose.yml &&\
	sed -i -e "s/claraapp_fe_env/claraapp_fe_qa/" docker-compose.yml &&\
	sed -i -e "s/claraapp_fe_image_env/claraapp_fe_image_qa/" docker-compose.yml &&\
	sed -i -e "s/3000\:80/3001\:80/" docker-compose.yml &&\
	sed -i -e "s/claraapp_fe_network_env/claraapp_fe_network_qa/g" docker-compose.yml
	@echo "Building new image for QA"
	docker-compose build --no-cache
	@echo "Starting container"
	docker-compose up -d
dev:
	sed -i -e "s/claraapp-fe-env/claraapp-fe-dev/" docker-compose.yml &&\
	sed -i -e "s/claraapp_fe_env/claraapp_fe_dev/" docker-compose.yml &&\
	sed -i -e "s/claraapp_fe_image_env/claraapp_fe_image_dev/" docker-compose.yml &&\
	sed -i -e "s/3000\:80/3001\:80/" docker-compose.yml &&\
	sed -i -e "s/claraapp_fe_network_env/claraapp_fe_network_dev/g" docker-compose.yml
	@echo "Building new image for DEV"
	docker-compose build --no-cache
	@echo "Starting container"
	docker-compose up -d
