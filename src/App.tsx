import React, { useEffect } from 'react';
import { ThemeProvider, Box } from '@mui/material';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import './App.css';
import AppRoutes from './routes';
import { IApp, IState } from './shared/utilities/interfaces';
import AppTheme from './core/theme/appTheme';
import Header from './core/components/header';
import Footer from './core/components/footer';
import Loader from './core/components/appLoader';
import Alert from 'core/components/alert';
import { history } from './store/store';
 
interface IRootApp {
	app: IApp;
}

const RootApp = (rootProps: IRootApp) => {
	const {
		app: {
			isLoading,
			isShowingModal
		},
	} = rootProps;
	const theme = AppTheme();
	const currentPath = history.location.pathname;
	useEffect(() => {
		window.scrollTo(0, 0);
	}, [currentPath]);
	return (
		<ThemeProvider theme={theme}>
			{isLoading ? <Loader /> : null}
			{isShowingModal && <Alert />}
			<Helmet>
				<title>Micro Bank</title>
			</Helmet>
			{!currentPath.includes('auth') ? <Header/> : null}
			<Box sx={{
				margin: 'auto',
				maxWidth: '100%',
				minHeight: 'calc(100vh - (4rem + 40px))',
			}}>
				<AppRoutes />
			</Box>
			{!currentPath.includes('auth') ? <Footer/> : null}
		</ThemeProvider>
	);
};

export default connect((state: IState) => ({
	app: state.app,
}))(RootApp);
