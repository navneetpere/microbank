import { connectRouter } from 'connected-react-router';
import { AnyAction, combineReducers } from 'redux';
import { IState } from '../shared/utilities/interfaces';
import Auth from '../features/auth/store/reducers';
import Core from '../core/store/app/reducers';
import { AUTH_ACTIONS } from '../features/auth/store/actions';
import UserHomePage from '../features/user/homePage/store/reducers';
import UserProfile from '../features/user/profile/store/reducers';
// HYGEN-IMPORT

const combinedReducers = (history: any) =>
  combineReducers({
    router: connectRouter(history),
    auth: Auth,
    app: Core,
    userHomePage: UserHomePage,
    userProfile: UserProfile,
  });// HYGEN-ADD-REDUCER
  // please do not remove this and above comments;
  // they are used by hygen for template generation

const createRootReducer =
  (history: any) => (state: IState, action: AnyAction) => {
    // Reset session state once user logout
    if (action.type === AUTH_ACTIONS.LOGOUT_REQUEST) {
      state = {} as IState;
    }
    return combinedReducers(history)(state, action);
  };

export default createRootReducer;
