// test-utils.js
import React from 'react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { createStore, compose, applyMiddleware } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import { Provider as ReduxProvider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { ThemeProvider } from 'styled-components';
import { persistReducer } from 'redux-persist';
import autoMergeLevel1 from 'redux-persist/lib/stateReconciler/autoMergeLevel1';
import storage from 'redux-persist/lib/storage/session';

import { history } from '../store/store';
import createRootReducer from '../store/reducer';
import theme from '../core/theme/appTheme';

global.fetch = require('jest-fetch-mock');
global.URL.createObjectURL = jest.fn();
global.open = jest.fn();
document.execCommand = jest.fn();

const persistConfig = {
	key: 'root',
	storage,
	stateReconciler: autoMergeLevel1
};

const persistedReducer = persistReducer(
	persistConfig,
	createRootReducer(history)
);

const customRender = (
	ui,
	{
		initialState,
		store = createStore(
			persistedReducer,
			initialState,
			compose(applyMiddleware(routerMiddleware(history), thunk))
		),
		...renderOptions
	} = {}
) => {
	const AllTheProviders = ({ children }) => {
		return (
			<ReduxProvider store={store}>
				<ThemeProvider theme={theme()}>
					<ConnectedRouter history={history}>{children}</ConnectedRouter>
				</ThemeProvider>
			</ReduxProvider>
		);
	};
	return render(ui, { wrapper: AllTheProviders, ...renderOptions });
};

// re-export everything
export * from '@testing-library/react';

// override render method
export { customRender as render };

const middlewares = [thunk];
export const mockStore = configureMockStore(middlewares);

export const mockServiceCreator =
	(body, succeeds = true) =>
		() =>
			new Promise((resolve, reject) => {
				setTimeout(() => (succeeds ? resolve(body) : reject(body)), 10);
			});
