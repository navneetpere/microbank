import React, { useState } from 'react';
import {
  FormControl,
  FormHelperText,
  IconButton,
  Input,
  InputAdornment,
  InputLabel,
  InputProps
} from '@mui/material';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import { Field } from 'formik';
import { get } from 'lodash';

interface IProps extends InputProps {
  label: string;
  variant?: 'filled' | 'outlined' | 'standard';
  helpertext?: string;
  validate?: any;
  isRequired?: boolean;
  errorHeight?: boolean;
}

function AuthInput(props: IProps) {
  const [showPassword, setShowPassword] = useState(false);
  const {
    label,
    variant = 'outlined',
    type,
    isRequired = false,
    errorHeight,
    ...inputProps
  } = props;

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };

  const getType = () => {
    if (type === 'password') {
      if (showPassword) {
        return 'text';
      }
      return 'password';
    }
    return type;
  };

  return (
    <Field name={props.name}>
      {({ field, form: { touched, errors, submitCount } }: any) => {
        let error = (field.value && get(errors, field.name)) ? get(errors, field.name) : '';
        if(!error) {
          error = get(touched, field.name) || submitCount
            ? get(errors, field.name)
            : false;
        }
        return (
          <FormControl fullWidth variant={variant} >
            {label.length ?
              <InputLabel
                sx={{
                  position: 'static',
                  color: 'secondary.main',
                  fontWeight: 'bold'
                }}
                htmlFor={props.id}
                focused={true}
                shrink={true}
              >
                {label} {isRequired ? <span className='required-asterisk'></span> : ''}
              </InputLabel>
            : null}
            <Input
              sx={{
                bgcolor: 'secondary.light',
                borderRadius: '12px',
                padding: '10px'
              }}
              {...inputProps}
              type={getType()}
              endAdornment={
                (type === 'password') && (
                  <InputAdornment
                    position='end'
                  >
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                      sx={{ m: 0, transform: 'translateY(-4px)' }}
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                )
              }
              onChange={field.onChange}
              onBlur={field.onBlur}
              name={field.name}
              value={field.value}
              error={Boolean(error)}
              variant="filled"
              {...field}
            />
            <FormHelperText id={props.id} sx={{ color: 'error.main', minHeight: errorHeight ? '19px' : 'auto' }}>
              {error}
            </FormHelperText>
          </FormControl>
        );
      }}
    </Field>
  );
};

export default AuthInput;