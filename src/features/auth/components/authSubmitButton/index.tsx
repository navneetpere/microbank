import React from 'react';
import { Button, ButtonProps, CircularProgress } from '@mui/material';
import { styled } from '@mui/system';

const AuthSubmitButton = styled(Button)(() => ({
  width: '100%',
  height: '50px',
  marginTop: '2rem',
  boxShadow: ' 0px 6px 28px 5px rgba(254, 45, 23, 0.3);'
}));

interface IAuthButton extends ButtonProps {
  isLoading?: boolean;
}

const AuthButton = (authButtonProps: IAuthButton) => {
  const { isLoading, children, variant } = authButtonProps;

  return (
    <AuthSubmitButton variant={variant} {...authButtonProps}>
      {isLoading
        ?
        <CircularProgress
          size={24}
          sx={{
            color: 'common.white'
          }}
        />
        : children
      }
    </AuthSubmitButton>
  );
};

export default AuthButton;