import React from 'react';
import { Route, useRouteMatch, Switch, Redirect } from 'react-router-dom';
import { styled, Box } from '@mui/system';
import Login from './login';
import { AuthContainer, StyledAuth } from './index.styles';
import BG from '../../../assets/auth.svg';
import BG_LEFT_TOP from '../../../assets/left.svg';
import BG_BOT_RIGHT from '../../../assets/right.svg';
import microBank from '../../../assets/MicroBank.svg';
import SignUp from './signup';


const InfoContainer = styled(Box)(({ theme }) => ({
  position: 'fixed',
  left: 0,
  top: 0,
  width: '50%',
  height: '100vh',
  display: 'flex',
  flexDirection: 'column',
  jsutifyContent: 'center',
  alignItems: 'center',
  '.info': { 
    marginLeft: '50px',
    marginTop: '-140px'
  },
  ":before" : {
		content: '""',
    background: `url(${BG_BOT_RIGHT})`,
		position: 'fixed',
    height: '227px', 
    width: '227px',
		left: '0',
	},
	":after" : {
    content: '""',
    background: `url(${BG_LEFT_TOP})`,
		position: 'fixed',
    height: '137px', 
    width: '137px',
		bottom: '16px',
    right: 'calc(50% + 24px);'
	},
  [theme.breakpoints.down(1025)]: {
    display: 'none'
  }
}));

function Auth() {
  const { path } = useRouteMatch();

  return (
    <>
      <AuthContainer>
      <InfoContainer sx={{ zIndex: 88 }}>
        <Box sx={{m: 'auto', mb: 0}} >
          <img src={BG} alt=""/>
        </Box>
        <Box sx={{m: 'auto', mt: 0}}>
          <img src={microBank} alt=""/>
        </Box>
      </InfoContainer> 
        <StyledAuth>
          <Box className='form-container'>
            <Switch>
              <Route exact path={path}>
                <Redirect to="/auth/login" />
              </Route>
              <Route path={`${path}/login`}>
                <Login />
              </Route>
              <Route path={`${path}/signup`}>
                <SignUp />
              </Route>
              <Redirect to="/auth/login" />
            </Switch>
          </Box>
        </StyledAuth>
      </AuthContainer>
    </>
  );
};

export default Auth;