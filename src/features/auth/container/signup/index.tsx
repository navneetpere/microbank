/* eslint-disable no-unused-vars */
import React from 'react';
import { connect } from 'react-redux';
import { Formik, FormikProps } from 'formik';
import FormikErrorFocus from 'formik-error-focus';

import RouteLink from '../../../../shared/components/routeLink';
import AuthInput from '../../components/authInput';
import AuthButton from '../../components/authSubmitButton';
import { enableModal } from '../../../../core/store/app/actions';
import { IModalData, IState } from '../../../../shared/utilities/interfaces';
import { AuthSubtitle, AuthTitle } from '../index.styles';
import { FieldType } from '../../../../shared/utilities/interfaces';
import { getValidation } from '../../../../shared/utilities/validationSchema';
import { loginAction } from '../../store/actions';
import { AccountStatusEnum, LoginFormBody } from 'features/auth/utilites';
import { Box } from '@mui/material';

interface ILogin {
  enableAppModal: (data: IModalData) => void;
  login: (data: LoginFormBody) => void;
  isLoggedIn: boolean;
  accountStatus: string;
};

const LoginFormFields: FieldType[] = [
	{
		field: 'name',
		label: 'Full Name',
		type: 'text',
		validation: {
				type: 'text',
				length: {
				max: 40,
				min: 0
				},
				required: true
		}
	},
  {
    field: 'email',
    label: 'Email',
    type: 'email',
    validation: {
      type: 'email',
      length: {
        max: 40,
        min: 0
      },
      required: true
    }
  },
	{
    field: 'date',
    label: 'Date of Incorporation',
    type: 'date',
    validation: {
      type: 'text',
      length: {
        max: 40,
        min: 0
      },
      required: true
    }
  },
  {
    field: 'password',
    label: 'Password',
    type: 'password',
    validation: {
      type: 'text',
      length: {
        max: 20,
        min: 8
      },
      required: true
    }
  },
  {
    field: 'confirmPassword',
    label: 'Confirm Password',
    type: 'password',
    validation: {
      type: 'text',
      length: {
        max: 20,
        min: 8
      },
      required: true
    }
  }
];

function Login(
  loginProps: ILogin
) {
  const { login, isLoggedIn, accountStatus, enableAppModal } = loginProps;
  const initialValues = {
    email: '', password: ''
  };

  const onSubmit = async (data: typeof initialValues) => {
    // call api for login
    // enableAppModal({
    //   alertTitle: 'Error',
    //   message: 'this is an error',
    //   severity: 'error',
    //   strongText: 'see here'
    // });
    const response = await login({
      email: data.email,
      password: data.password
    });
  };

  return (<div style={{maxWidth: '320px'}}>
    <AuthTitle variant='h4' fontWeight="bold">
			Create an Account
    </AuthTitle>
    <AuthSubtitle
      variant='body1'
      fontWeight='bold'
    >
      Please Login to your account&nbsp;
    </AuthSubtitle>
    <Formik
      initialValues={initialValues}
      validationSchema={getValidation(LoginFormFields)}
      onSubmit={onSubmit}
      enableReinitialize
      validateOnBlur
    >
      {({ handleSubmit }: FormikProps<typeof initialValues>) => (
        <form onSubmit={(e: React.FormEvent<HTMLFormElement>) => {
          e.preventDefault();
          handleSubmit();
        }}>
					<AuthInput
						variant='filled'
						label='Full Name'
						name='name'
					/>
					<AuthInput
            variant='filled'
            label='Date of Incorporation'
            name='date'
						type='date'
          />
          <AuthInput
            variant='filled'
            label='Email'
            name='email'
          />					
          <AuthInput
            variant='filled'
            label='Password'
            name='password'
            type='password'
          />
          <AuthInput
            variant='filled'
            label='Confirm Password'
            name='confirmPassword'
            type='password'
          />
          <AuthButton variant='contained' type='submit' sx={{mb: '2rem'}}>
						Create an Account
          </AuthButton>
          <Box sx={{textAlign: 'center'}}>
            <AuthSubtitle
              fontWeight='bold'
              variant='body1'
            >
              Already have an account?&nbsp;
            </AuthSubtitle>
            <RouteLink
              fontWeight='bold'
              to='/auth/login'
            >
              Login
            </RouteLink>
          </Box>
          <FormikErrorFocus
            offset={0}
            align="middle"
            focusDelay={10}
            ease="linear"
            duration={500}
          />
        </form>
      )}
    </Formik>
  </div>);
}

export default connect((state: IState) => ({
  isLoggedIn: state.auth.isLoggedIn,
  accountStatus: state.auth.status
}),
  {
    enableAppModal: enableModal,
    login: loginAction
  }
)(Login);
