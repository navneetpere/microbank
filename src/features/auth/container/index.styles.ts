import { styled, Box } from '@mui/system';
import { Typography } from '@mui/material';

export const StyledAuth = styled(Box)(({ theme }) => ({
  gridColumnStart: '2',
  background: 'white',
  display: 'flex',
  alignItems: 'center',
  width: '100%',
  justifyContent: 'center',
  minHeight: '-webkit-fill-available',
  boxShadow: '0px 7px 8px -4px rgba(0, 0, 0, 0.2), 0px 13px 19px 2px rgba(0, 0, 0, 0.14), 0px 40px 40px 40px rgba(0, 0, 0, 0.12)',
  [theme.breakpoints.down(1025)]: {
    gridColumnStart: '1',
    maxWidth: '80%',
    width: '80%',
    minWidth: '280px',
    minHeight: '340px',
    '.form-container': {
      maxWidth: '80%',
      minWidth: '80%'
    },
  }
}));

export const AuthContainer = styled(Box)(({ theme }) => ({
  position: 'absolute',
  width: '100%',
  display: 'grid',
  gridTemplateColumns: '1fr 1fr',
  gridTemplateRows: '1fr',
  justifyContent: 'center',
  alignContent: 'center',
  justifyItems: 'center',
  alignItems: 'center',
  backgroundAttachment: 'fixed',
  backgroundPosition: 'center',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  backgroundColor: '#FE2D17',
  minHeight: '100%',
  [theme.breakpoints.down(1025)]: {
    gridTemplateColumns: '1fr',
    position: 'static',
    minHeight: '100%',
  }
}));

export const AuthTitle = styled(Typography)(() => ({
  marginBottom: '1rem',
  fontSize: '32px',
  lineHeight: '37px'
}));

export const AuthSubtitle = styled(Typography)(({ theme }) => {
  return {
    marginBottom: '2rem',
    color: theme.palette.claraPalette.gray1,
    fontSize: '14px',
    lineHeight: '16px',
    display: 'inline-block',
    '.em': {
      color: theme.palette.primary.main
    }
  };
});
