import React, {useState} from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import VisibilityIcon from '@mui/icons-material/Visibility';
import DownloadIcon from '@mui/icons-material/Download';
import SummaryCard from '../../components/summaryCard';
import { StyledPaper } from '../../container/index.style';
import { IconButton } from '@mui/material';

function createData(
  tid: string,
  date: string,
  amount: string,
  status: string
) {
  return { tid, date, amount, status };
}

const rows = [
  createData('2011002039111', '22-09-2021', '1,780', 'Approved'),
  createData('2011002039112', '19-09-2021', '1,890', 'Approved'),
  createData('2011002039113', '20-08-2021', '5,500', 'Rejected'),
  createData('2011002039114', '12-07-2021', '2,590', 'Approved'),
  createData('2011002039116', '07-07-2021', '2,800', 'Rejected'),
];


export default function BasicTable() {
    const [open, setOpen] = useState(false);
    const [currentTransaction, setCT] = useState({
        tid: '',
        date: '',
        amount: '',
        status: ''
    });
    const handleClick = (data) => {
        setCT(data);
        setOpen(!open);
    };
    return (
    <StyledPaper 
        sx={{
            mt: '12px',
            p: '0.5rem 1rem'
        }}
    >   
        <TableContainer>
        <Table sx={{ minWidth: 650, maxWidth: 'calc(100% - 2px)' }} size="small" aria-label="simple table">
            <TableHead>
                <TableRow>
                    <TableCell sx={{color: 'claraPalette.gray1', fontWeight: '700'}}>Transaction ID</TableCell>
                    <TableCell sx={{color: 'claraPalette.gray1', fontWeight: '700'}} align="left">Date</TableCell>
                    <TableCell sx={{color: 'claraPalette.gray1', fontWeight: '700'}} align="left">Amount</TableCell>
                    <TableCell sx={{color: 'claraPalette.gray1', fontWeight: '700'}} align="left">Status</TableCell>
                    <TableCell sx={{color: 'claraPalette.gray1', fontWeight: '700'}} align="left">Action</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
            {rows.map((row) => (
                <TableRow
                key={row.tid}
                sx={{
                    bgcolor: 'rgba(244, 245, 253, 0.5)',
                    border: '1px solid #E0E0E0',
                    '&:first-child td, &:first-child th': {borderRadius: '4px 4px 0px 0px' },
                    '&:last-child td, &:last-child th': {borderRadius: '0px 0px 4px 4px' }
                }}
                >
                <TableCell component="th" scope="row">
                    {row.tid}
                </TableCell>
                <TableCell align="left">{row.date}</TableCell>
                <TableCell align="left">{row.amount}</TableCell>
                <TableCell
                    align="left"
                    sx={{
                        fontWeight: 400,
                        color: row.status === 'Approved' ? 'success.light' : 'primary.main'}}
                >
                    {row.status}
                </TableCell>
                <TableCell align="left">
                    <IconButton onClick={() => handleClick(row)} sx={{p: '0 22px 0 0', m: 0}}>
                        <VisibilityIcon sx={{color: '#E8544B'}} />
                    </IconButton>
                    <IconButton onClick={handleClick} sx={{p: 0, m: 0}}>
                        <DownloadIcon sx={{color: '#E8544B'}}/>
                    </IconButton>
                </TableCell>
                </TableRow>
            ))}
            </TableBody>
        </Table>
        </TableContainer>
        <SummaryCard
            dialogOpen={open}
            closeDialog={() => setOpen(false)}
            transaction={currentTransaction}
            Action={() => (
                <IconButton onClick={handleClick} sx={{p: 0, m: 0}}>
                    <DownloadIcon sx={{color: '#E8544B'}}/>
                </IconButton>
            )}
        />
    </StyledPaper>
  );
};