import React from 'react';
import SummaryData from '../../components/summaryData';
import CurrentServices from '../../components/currentServices';
import AvailableServices from '../../components/availableServices';
// import { Typography } from '@mui/material';
// import { StyledPaper } from '../../container/index.style';

function UserHomePage () {
  return (
    <>
        <SummaryData></SummaryData>
        <CurrentServices></CurrentServices>
        <AvailableServices></AvailableServices>
    </>
  );
};

export default UserHomePage;