import React, { useState } from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { ProfileContainer } from '../../profile/container/index.style';
import { AuthTitle } from '../../../auth/container/index.styles';
import { Box } from '@mui/material';
import Summary from './summary';
import Details from './details';
import AppTheme from '../../../../core/theme/appTheme';
import { StyledPaper } from './index.style';

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      {...other}
    >
      {value === index && (
        <Box>
          {children}
        </Box>
      )}
    </div>
  );
}

function UserHomePage () {
  const [value, setValue] = useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };
	const theme = AppTheme();
  const tabTheme: any = {
    textTransform: 'none',
    fontWeight: theme.typography.fontWeightRegular,
    color: '#232F34',
    border: 'none',
    backgroundColor: 'transparent',
    '&.Mui-selected': {
      color: theme.palette.secondary.main ,
      fontWeight: theme.typography.fontWeightBold,
    },
  };
  return (
    <ProfileContainer>
      <AuthTitle variant='h4' fontWeight="bold" sx={{mb: '1.5rem'}}>
        Dashboard
      </AuthTitle>
      <Box sx={{ width: '100%' }}>
        <StyledPaper sx={{ overflow: 'hidden' }}>
          <Tabs value={value} onChange={handleChange} sx={{
            '& .MuiTabs-indicator': {
              position: 'absolute',
              height: '3px',
              top: 0,
              backgroundColor: theme.palette.primary.main,
            },
          }}>
            <Tab label="Summary" sx={tabTheme} />
            <Tab label="Details" sx={tabTheme} />
          </Tabs>
        </StyledPaper>
        <TabPanel value={value} index={0}>
          <Summary />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Details />
        </TabPanel>
      </Box>
    </ProfileContainer>
  );
};

export default UserHomePage;