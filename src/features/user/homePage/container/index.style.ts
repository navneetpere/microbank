import { Paper } from '@mui/material';
import { styled } from '@mui/system';

export const StyledPaper = styled(Paper)(() => ({
 boxShadow:' 0px 1px 5px rgba(0, 0, 0, 0.12)',
}));