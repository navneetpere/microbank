import React from 'react';
import { Dialog, Divider, IconButton, Paper, Stack, Typography, Grid } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { Box } from '@mui/system';

const Topic = ({label}) => (
    <Typography
        variant="subtitle2"
        fontWeight='bold'
        sx={{
            lineHeight: "22px",
            color: "claraPalette.gray1",
            mt: '1.5rem'
        }}
    >
        {label}
    </Typography>
);

const Data = ({label}) => (
    <Typography
        variant="subtitle2"
        sx={{
            fontWeight: 400,
            lineHeight: "22px",
            color: "#232F34",
        }}
    >
        {label}
    </Typography>
);

function SuggestBrandPopup({
    dialogOpen, closeDialog, transaction, Action
}) {
    return (
        <Dialog open={dialogOpen} onClose={closeDialog}>
            <Paper
                sx={{
                    padding: '1.5rem',
                    width: "480px",
                }}
            >
                <Stack
                    direction="row"
                    justifyContent="space-between"
                    sx={{
                        pb: "1.5rem",
                    }}
                >
                    <Typography
                        sx={{
                            fontWeight: 700,
                            fontSize: "16px",
                            lineHeight: "28px",
                            color: "#232F34",
                        }}
                    >
                        Transaction Details
                    </Typography>
                    <IconButton sx={{ m: 0, p: 0 }}>
                        <CloseIcon
                            sx={{ color: "claraPalette.gray2" }}
                            onClick={closeDialog}
                        />
                    </IconButton>
                </Stack>
                <Divider />
                <Box>
                    <Grid container>
                        <Grid item xs={6}>
                            <Topic label="Transaction ID" />
                            <Data label={transaction.tid} />

                            <Topic label="Date" />
                            <Data label={transaction.date} />

                            <Topic label="Status" />
                            <Data label={transaction.status} />

                            <Topic label="Comment" />
                            <Data label="Against invoice IN00021009" />
                        </Grid>
                        <Grid item xs={6}>
                            <Topic label="Transfer to" />
                            <Data label="XYS corporation" />

                            <Topic label="Amount" />
                            <Data label={transaction.amount} />

                            <Topic label="Action" />
                            <Action />

                        </Grid>
                    </Grid>
                </Box>
            </Paper>
        </Dialog>
    );
}

export default SuggestBrandPopup;