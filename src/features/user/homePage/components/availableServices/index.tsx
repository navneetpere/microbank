import React from 'react';
import { Typography, Box } from '@mui/material';
import { StyledPaper } from '../../container/index.style';
import {
    Payment, Tax, Investment, Wallet
} from '../../../../../shared/components/icons';

const Services = ({title, icon}) => (
    <Box sx={{
        width: '218px',
        mt: '1.5rem',
        mb: '6.5px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-end'
    }}>
        <Box>
            {icon}
        </Box>
        <Typography sx={{ mt: '1rem' , lineHeight: '24.5px'}} variant="body2">
            {title}
        </Typography>
    </Box>
);

function SummaryData () {
  return (
        <StyledPaper sx={{
            p: '1.5rem',
            m: '0.75rem 0 0 0',
        }}>
            <Typography
                fontWeight='bold'
                color='secondary'
                variant='subtitle1'
                component='div'
            >
                Current Service
            </Typography>
            <Box sx={{display: 'flex'}}>
                <Services 
                    title="Wallet" 
                    icon={<Wallet />} 
                />
                <Services 
                    title="Investment" 
                    icon={<Investment />} 
                />
                <Services 
                    title="Tax Payment" 
                    icon={<Tax />} 
                />
                <Services 
                    title="Loan" 
                    icon={<Payment />} 
                />
            </Box>
        </StyledPaper>
  );
};

export default SummaryData;