import React from 'react';
import { Typography, Box } from '@mui/material';
import { StyledPaper } from '../../container/index.style';
import {
    Payroll,
    Account,
    Payment
} from '../../../../../shared/components/icons';
import Rating from '@mui/material/Rating';

const Services = ({title, icon, rating}) => (
    <Box sx={{
        width: '218px',
        mt: '1.5rem',
        mb: '6.5px',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-end'
    }}>
        <Box>
            {icon}
        </Box>
        <Typography sx={{ mt: '1rem' , lineHeight: '24.5px'}} variant="body2">
            {title}
        </Typography>
        <Rating value={rating} sx={{color: '#0A8B1F'}} readOnly />
    </Box>
);

function SummaryData () {
  return (
        <StyledPaper sx={{
            p: '1.5rem',
            m: '0.75rem 0 0 0',
        }}>
            <Typography
                fontWeight='bold'
                color='secondary'
                variant='subtitle1'
                component='div'
            >
                Current Service
            </Typography>
            <Box sx={{display: 'flex'}}>
                <Services 
                    title="Current Account" 
                    icon={<Account />} 
                    rating={3}
                />
                <Services 
                    title="Payroll" 
                    icon={<Payroll />} 
                    rating={2}
                />
                <Services 
                    title="Payments" 
                    icon={<Payment />} 
                    rating={0}
                />
            </Box>
        </StyledPaper>
  );
};

export default SummaryData;