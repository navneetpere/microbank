import React from 'react';
import { Typography, Box } from '@mui/material';
import { StyledPaper } from '../../container/index.style';

const DataBox = ({title, data}) => (
    <Box sx={{width: '20rem', mt: '1.5rem', mb: '6.5px'}}>
        <Typography sx={{ mb: 1 , lineHeight: 0}} color="#00000038" variant="body2">
            {title}
        </Typography>
        <Typography variant="subtitle1" fontWeight="bold" color="claraPalette.gray6">
            {data}
        </Typography>
    </Box>
);

function SummaryData () {
  return (
        <StyledPaper sx={{
            p: '1.5rem',
            m: '0.75rem 0 0 0',
            background: '#FFF0F3',
            border: '1px solid rgba(217, 4, 41, 0.2)',
        }}>
            <Typography
                fontWeight='bold'
                color='secondary'
                variant='subtitle1'
                component='div'
            >
                Summary
            </Typography>
            <Box sx={{display: 'flex'}}>
                <DataBox title="Account Number" data="67238744"/>
                <DataBox title="Income" data="$20,00,000"/>
                <DataBox title="Spends" data="$11,00,000"/>
            </Box>
        </StyledPaper>
  );
};

export default SummaryData;