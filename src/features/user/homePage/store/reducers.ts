import { ReducerActionProps } from '../../../../shared/utilities/interfaces';
import {
  GET_USER_HOMEPAGE_DATA
} from './actions';

const initialState = {
  // add initial state variables
};

const userHomePageReducer = (state = initialState, action: ReducerActionProps) => {
  switch (action.type) {
    case GET_USER_HOMEPAGE_DATA:
      return {
        ...state,
        ...action.payload
      };

    default:
      return {
        ...state
      };
  }
};

export default userHomePageReducer;
