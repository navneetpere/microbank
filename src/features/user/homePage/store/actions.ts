import { AppDispatch } from '../../../../store/store';

export const GET_USER_HOMEPAGE_DATA = 'GET_HOMEPAGE_DATA';

export function GetUserHomePage () {
  return (dispatch: AppDispatch) => {
    // perform actions
    dispatch({
      type: GET_USER_HOMEPAGE_DATA,
      payload: {
        // add payload here
      },
    });
  };
};
