import { styled, Box } from '@mui/system';

export const ProfileContainer = styled(Box)(({ theme }) => ({

  backgroundColor: theme.palette.claraPalette.gray5,
	padding: '1.5rem',
	marginTop: '3.9rem'
}));