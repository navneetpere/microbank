/* eslint-disable no-unused-vars */
import React from 'react';
import { connect } from 'react-redux';
import { Formik, FormikProps } from 'formik';
import FormikErrorFocus from 'formik-error-focus';
import { Box, Paper } from '@mui/material';

import RouteLink from '../../../../shared/components/routeLink';
import AuthInput from '../../../auth/components/authInput';
import AuthButton from '../../../auth/components/authSubmitButton';
import { enableModal } from '../../../../core/store/app/actions';
import { IModalData, IState } from '../../../../shared/utilities/interfaces';
import { FieldType } from '../../../../shared/utilities/interfaces';
import PhotoUpload from '../../../../shared/components/photoUpload';
import { getValidation } from '../../../../shared/utilities/validationSchema';
import { LoginFormBody } from 'features/auth/utilites';
import { ProfileContainer } from './index.style';
import { AuthTitle } from 'features/auth/container/index.styles';

interface ILogin {
  enableAppModal: (data: IModalData) => void;
  isLoggedIn: boolean;
  accountStatus: string;
};

const LoginFormFields: FieldType[] = [
	{
		field: 'name',
		label: 'Company Name',
		type: 'text',
		validation: {
				type: 'text',
				length: {
				max: 40,
				min: 0
				},
				required: true
		}
	},
  {
    field: 'email',
    label: 'Email Address',
    type: 'email',
    validation: {
      type: 'email',
      length: {
        max: 40,
        min: 0
      },
      required: true
    }
  },
  {
    field: 'password',
    label: 'Old Password',
    type: 'password',
    validation: {
      type: 'text',
      length: {
        max: 20,
        min: 8
      },
      required: true
    }
  },
  {
    field: 'newPassword',
    label: 'New Password',
    type: 'password',
    validation: {
      type: 'text',
      length: {
        max: 20,
        min: 8
      },
      required: true
    }
  },
  {
    field: 'confirmPassword',
    label: 'Confirm Password',
    type: 'password',
    validation: {
      type: 'text',
      length: {
        max: 20,
        min: 8
      },
      required: true
    }
  }
];

function Login(
  loginProps: ILogin
) {
  const { isLoggedIn, accountStatus, enableAppModal } = loginProps;
  const initialValues = {
    email: '', password: ''
  };

  const onSubmit = async (data: typeof initialValues) => {
    // call api for login
    // enableAppModal({
    //   alertTitle: 'Error',
    //   message: 'this is an error',
    //   severity: 'error',
    //   strongText: 'see here'
    // });
  };

  return (<ProfileContainer>
    <AuthTitle variant='h4' fontWeight="bold" sx={{mb: '1.5rem'}}>
      Edit Profile
    </AuthTitle>
    <Paper
      sx={{
        bgcolor: 'white',
        p: '1.5rem'
      }}
    >
      <Formik
        style={{backgroundColor: 'white'}}
        initialValues={initialValues}
        validationSchema={getValidation(LoginFormFields)}
        onSubmit={onSubmit}
        enableReinitialize
        validateOnBlur
        >
        {({ handleSubmit }: FormikProps<typeof initialValues>) => (
          <form
            style={{maxWidth: '320px'}}
            onSubmit={(e: React.FormEvent<HTMLFormElement>) => {
              e.preventDefault();
              handleSubmit();
            }}
            >
            <PhotoUpload />
            <AuthInput
              variant='filled'
              label='Company Name'
              name='name'
            />
            <AuthInput
              variant='filled'
              label='Email Address'
              name='email'
              />					
            <AuthInput
              variant='filled'
              label='Old Password'
              name='password'
              type='password'
            />
            <AuthInput
              variant='filled'
              label='New Password'
              name='newPassword'
              type='password'
              />
            <AuthInput
              variant='filled'
              label='Confirm Password'
              name='confirmPassword'
              type='password'
            />
            <AuthButton variant='contained' type='submit' sx={{mb: '2rem'}}>
              Update
            </AuthButton>
            <FormikErrorFocus
              offset={0}
              align="middle"
              focusDelay={10}
              ease="linear"
              duration={500}
            />
          </form>
        )}
      </Formik>
    </Paper>
  </ProfileContainer>
  );
}

export default connect((state: IState) => ({
  isLoggedIn: state.auth.isLoggedIn,
  accountStatus: state.auth.status
}),
  {
    enableAppModal: enableModal
  }
)(Login);
