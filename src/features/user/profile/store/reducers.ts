import { ReducerActionProps } from '../../../../shared/utilities/interfaces';
import {
  GET_USER_PROFILE_DATA
} from './actions';

const initialState = {
  // add initial state variables
};

const userProfileReducer = (state = initialState, action: ReducerActionProps) => {
  switch (action.type) {
    case GET_USER_PROFILE_DATA:
      return {
        ...state,
        ...action.payload
      };

    default:
      return {
        ...state
      };
  }
};

export default userProfileReducer;
