import { AppDispatch } from '../../../../store/store';

export const GET_USER_PROFILE_DATA = 'GET_PROFILE_DATA';

export function GetUserProfile () {
  return (dispatch: AppDispatch) => {
    // perform actions
    dispatch({
      type: GET_USER_PROFILE_DATA,
      payload: {
        // add payload here
      },
    });
  };
};
