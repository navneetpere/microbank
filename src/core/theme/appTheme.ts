import { createTheme } from '@mui/material';
import { IState } from '../../shared/utilities/interfaces';
import store from '../../store/store';
import OpenSans from '../../assets/font/OpenSans-Regular.ttf';
import OpenSansBold from '../../assets/font/OpenSans-Bold.ttf';
import OpenSansLight from '../../assets/font/OpenSans-Light.ttf';
import OpenSansItalic from '../../assets/font/OpenSans-Italic.ttf';
import OpenSansBoldItalic from '../../assets/font/OpenSans-BoldItalic.ttf';

const Theme = () => {
	const appState: IState = store.getState();
	const { themeMode } = appState.app;

	return createTheme({
		typography: {
			fontFamily: 'OpenSans, sans-serif',
		},
		palette: {
			...(themeMode === 'light'
				? {
					primary: {
						main: '#D90429'
					},
					secondary: {
						contrastText: '#828282',
						light: '#E8E8E8',
						main: '#1A1A1A'
					},
					error: {
						main: '#EB5757',
					},
					success: {
						main: '#27AE60',
						light: '#008000',
						contrastText: '#219653'
					},
					warning: {
						main: '#F2994A'
					},
					claraPalette: {
						darkGray: '#1A1A1A',
						gray1: '#828282',
						gray2: '#BDBDBD',
						gray3: '#E0E0E0',
						gray4: '#F2F2F2',
						gray5: '#E5E5E5',
						gray6: '#333333'
					}
				}
				: {
					primary: {
						main: '#BDBDBD'
					}
				})
		},
		components: {
			MuiCssBaseline: {
				styleOverrides: `
					@font-face {
						font-family: 'OpenSans, sans-serif';
						font-style: normal;
						font-weight: 400;
						font-display: swap;
						src: local('OpenSans, sans-serif'), url(${OpenSans}) format('truetype');
					}
					@font-face {
						font-family: 'OpenSans, sans-serif';
						font-style: normal;
						font-weight: 700;
						font-display: swap;
						src: local('OpenSans, sans-serif'), url(${OpenSansBold}) format('truetype');
					}
					@font-face {
						font-family: 'OpenSans, sans-serif';
						font-style: italic;
						font-weight: 400;
						font-display: swap;
						src: local('OpenSans, sans-serif'), url(${OpenSansItalic}) format('truetype');
					}
					@font-face {
						font-family: 'OpenSans, sans-serif';
						font-style: italic;
						font-weight: 700;
						font-display: swap;
						src: local('OpenSans, sans-serif'), url(${OpenSansBoldItalic}) format('truetype');
					}
					@font-face {
						font-family: 'OpenSans, sans-serif';
						font-style: normal;
						font-weight: 300;
						font-display: swap;
						src: local('OpenSans, sans-serif'), url(${OpenSansLight}) format('truetype');
					}
				`
			},
			MuiIconButton: {
				styleOverrides: {
					root: {
						margin: '0 1rem'
					}
				}
			},
			MuiButton: {
				styleOverrides: {
					root: {
						borderRadius: '12px',
						textTransform: 'none',
						fontWeight: 700,
						'&.loading': {

						},

					},
					text: {
						color: 'black',
						':hover': {
							boxShadow: '0px 8px 9px -5px rgba(0, 0, 0, 0.2)'
						}
					}
				}
			},
			MuiPaper: {
				styleOverrides: {
					root: {
						borderRadius: '12px'
					}
				}
			},
			MuiSelect: {
				styleOverrides: {
					nativeInput: {
						borderBottom: 'none'
					},
					icon: {
						color: 'rgba(0, 0, 0, 0.26)'
					},
					select: {
						'&:focus':{
							backgroundColor:'white'
						  }
					},
					root: {
						'.required-asterisk': {
							color: '#EB5757',
							'::after': {
								'content': '"  *"'
							}
						}
					}
				}
			},
			MuiInput: {
				styleOverrides: {
					root: {
						borderBottomColor: 'none !important',
						borderBottomWidth: 0,
						'::before': {
							borderBottomColor: 'none !important',
							borderBottomWidth: 0,
						},
						'::after': {
							borderBottomColor: 'none !important',
							borderBottomWidth: 0,
						}
					},
					input: {
						fontSize: '16px',
						lineHeight: '19px',
						fontWeight: 'normal',
						color:'#333333'
					}
				}
			},
			MuiInputLabel: {
				styleOverrides: {
					root: {
						fontcolor: '#1A1A1A',
						'&.Mui-focused': {
							color: '#1A1A1A'
						},
						'.required-asterisk': {
							color: '#EB5757',
							'::after': {
								'content': '"  *"'
							}
						}
					}
				}
			},
			MuiTypography: {
				styleOverrides: {
					root: {
						'&.em': {
							color: '#D90429'
						},
						'.required-asterisk': {
							color: '#EB5757',
							'::after': {
								'content': '"  *"'
							}
						}
					},
					h1: {
						fontSize: '6rem'
					},
					h2: {
						fontSize: '3.75rem'
					},
					h3: {
						fontSize: '3rem'
					},
					h4: {
						fontSize: '2rem'
					},
					h5: {
						fontSize: '1.5rem'
					},
					h6: {
						fontSize: '1.25rem'
					},
					subtitle1: {
						fontSize: '1rem'
					},
					subtitle2: {
						fontSize: '0.875rem'
					},
					body1: {
						fontSize: '0.875rem'
					},
					body2: {
						fontSize: '0.75rem'
					}
				}
			}
		}
	});
};

export default Theme;
