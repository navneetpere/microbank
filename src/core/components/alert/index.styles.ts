import { Alert } from '@mui/material';
import { Box, styled } from '@mui/system';

export const AlertContainer = styled(Box)(() => ({
  display: 'flex',
  width: '100vw',
  justifyContent: 'center'
}));

export const StyledAlert = styled(Alert)(({ theme, severity }) => {
  return {
    color: severity === 'error'
      ? theme.palette.error.main
      : severity === 'info'
        ? theme.palette.info.main
        : severity === 'success'
          ? theme.palette.success.main
          : severity === 'warning'
            ? theme.palette.warning.main
            : 'inherit',
    boxShadow: theme.shadows[2],
    position: 'fixed',
    marginTop: '2rem',
    zIndex: theme.zIndex['modal'] + 1
  };
});