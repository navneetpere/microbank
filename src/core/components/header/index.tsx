import React from 'react';
import {
	AppBar,
	Avatar,
	IconButton,
	Menu,
	MenuItem,
	Toolbar,
	Typography
} from '@mui/material';
import { Box, styled } from '@mui/system';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { connect } from 'react-redux';
import NotificationsActiveIcon from '@mui/icons-material/NotificationsActive';

import { toggleAppTheme } from '../../store/app/actions';
import { IApp, IAuth, IState } from '../../../shared/utilities/interfaces';
import Apptheme from '../../theme/appTheme';
import ExitIcon from '../../../assets/exit_Icon.svg';
import Micro from '../../../assets/head.svg';
import { logoutAction } from '../../../features/auth/store/actions';


const HeaderContainer = styled(AppBar)(({ theme }) => {
	return {
		backgroundColor: theme.palette.common.white,
		borderRadius: '0',
		zIndex: theme.zIndex['appBar'],
		boxShadow: '0px 1px 5px rgba(0, 0, 0, 0.12)'
	};
});
interface IHeader {
	app: IApp;
	// eslint-disable-next-line no-unused-vars
	changeAppTheme: (theme: string) => void;
	auth: IAuth;
	logout: () => void;
}

const Header = (HeaderProps: IHeader) => {
	const theme = Apptheme();
	const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
	const { logout } = HeaderProps;
	const open = Boolean(anchorEl);

	const handleClick = (event: React.MouseEvent<HTMLElement>) => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	return (
		<>
			<Box sx={{ flexGrow: 1 }}>
				<HeaderContainer
					position='fixed'
				>
					<Toolbar>
						<img src={Micro} alt="" style={{
								marginRight: '2rem',
						}}/>
						<Typography
							color='secondary'
							variant='h5'
							component='div'
							sx={{
								pl: '2rem',
								borderLeft: '1px solid #c4c4c4',
								flexGrow: 1,
								fontWeight: 700,
								userSelect: 'none'
							}}
						>
							ABC Pvt Ltd
						</Typography>
						<Box
							onClick={handleClick}
							style={{
								cursor: 'pointer',
								display: 'flex',
								alignItems: 'center',
							}}
						>
							<IconButton
								aria-label="more"
								id="long-button"
								aria-controls="long-menu"
								aria-expanded={open ? true : null}
								aria-haspopup="true"
								onClick={handleClick}
								style={{
									padding: '0',
									margin: '0 26.02px 0 0',
								}}
							>
							<NotificationsActiveIcon style={{
								color: 'rgba(0, 0, 0, 1)'
							}} />
							</IconButton>
							<Avatar
								sx={{ bgcolor: theme.palette.primary.main }}
								style={{
									width: '28px',
									height: '28px',
									fontSize: '14px',
									cursor: 'pointer',
									textTransform: 'capitalize'
								}}
								onClick={handleClick}
							>
								PT
							</Avatar>
							<IconButton
								aria-label="more"
								id="long-button"
								aria-controls="long-menu"
								aria-expanded={open ? true : null}
								aria-haspopup="true"
								onClick={handleClick}
								style={{
									padding: '0',
									margin: '0 0 0 3px',
								}}
							>
								<ArrowDropDownIcon style={{
									color: 'rgba(0, 0, 0, 0.26)'
								}} />
							</IconButton>
						</Box>
						<Menu
							sx={{
								mt: '12px'
							}}
							id="long-menu"
							MenuListProps={{
								'aria-labelledby': 'long-button',
							}}
							anchorEl={anchorEl}
							open={open}
							onClose={handleClose}
							PaperProps={{
								style: {
									borderRadius: '0px 0px 8px 8px',
									maxHeight: 48 * 4.5,
									width: '20ch',
									boxShadow: '0px 5px 6px -3px rgba(35, 47, 52, 0.2), 0px 9px 12px 1px rgba(35, 47, 52, 0.14)',
								},
							}}
						>
							<MenuItem
								sx={{
									fontSize: '12px',
									color: '#1A1A1A'
								}}
								onClick={() => {
									handleClose();
									logout();
								}}
							>
								<img src={ExitIcon} style={{ marginRight: '8px' }} /> Logout
							</MenuItem>
						</Menu>
					</Toolbar>
				</HeaderContainer>
			</Box>
		</>
	);
};

export default connect(
	(state: IState) => ({
		app: state.app,
		auth: state.auth
	}),
	{
		changeAppTheme: toggleAppTheme,
		logout: logoutAction
	}
)(Header);
