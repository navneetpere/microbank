import React from 'react';
import { Typography } from '@mui/material';
import { Box, styled } from '@mui/system';

const FallbackContainer = styled(Box)(() => ({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '100vh',
  width: '100vw',
  position: 'absolute'
}));

function LazyFallback() {
  return (<FallbackContainer>
    <Typography variant='h1'>Micro Bank</Typography>
  </FallbackContainer>);
}

export default LazyFallback;