import { Typography } from '@mui/material';
import React from 'react';
import {connect} from 'react-redux';
import { IState } from 'shared/utilities/interfaces';
// interface IFooter {
// 	auth: IAuth;
// }

function Footer() {
	return (
	<footer>
		<Typography
			variant='body2'
			sx={{
				bgcolor: '#E5E5E5',
				textAlign: 'center',
				p: '0.75rem',
				color: '#1A1A1A'
			}}
			style={{
				bottom: 0,
			}}
		>
			© 2021 Micro Bank
		</Typography>
	</footer>
	);
};

export default connect(
	(state: IState) => ({
		app: state.app,
		auth: state.auth
	})
)(Footer);
