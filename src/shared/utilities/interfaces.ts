/* eslint-disable no-unused-vars */
import { RouterState } from 'connected-react-router';
import { AlertColor } from '@mui/material';
import { AppDispatch } from '../../store/store';
import { AccountType } from 'core/constants';

export interface ReducerActionProps {
  type: string;
  payload?: any;
}

export type FunctionDispatchType = (dispatch: AppDispatch) => void;
export type ActionDispatchType = (dispatch: any) => Promise<void>;

export type DispatchType = (
  args: ReducerActionProps | FunctionDispatchType
) => void;

export interface IModalData {
  message: string | string[];
  severity: AlertColor;
  alertTitle: string;
  strongText?: string;
}

export interface IApp {
  themeMode: string;
  isLoading: boolean;
  info: unknown;
  isShowingModal: boolean;
  modalData: IModalData;
  userHomePage: UserHomePage,
  userProfile: UserProfile,
}

export interface UserHomePage  {}
export interface UserProfile  {}
export interface IAuth {
  status: string;
  isLoggedIn: boolean;
  accessToken: string;
  refreshToken: string;
  userId: string;
  sessionExpired: any;
  accountType: string;
  userData: any;
}

export interface IState {
  app: IApp;
  router: RouterState;
  auth: IAuth;
  userHomePage: any;
  userProfile: any;
} // HYGEN-UPDATE

export type inputFieldType = "regex" | "text" | "url" | "email";

export interface ValidationType {
  length: {
    min?: number;
    max?: number;
    exact?: number;
  };
  regex?: RegExp;
  regexMessage?: string;
  required: boolean;
  type: inputFieldType;
}

export interface FieldType {
  field: string;
  label: string;
  validation: ValidationType;
  type: string;
}