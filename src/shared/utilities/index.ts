export const getFormattedNumber = (num: number) => {
  return new Intl.NumberFormat('en-IN', {
    notation: 'compact',
    compactDisplay: 'short',
    // style: '',
    // currency: ''
  }).format(num).replace('T', 'K');
};

export const getFormattedUSNumber = (number) => {
  if(!number) return number;
  let num = '';
  let str = number.toString().replace(' ','').replace('.','').replace(',','');
  if(str.length > 16) {
    while(str.length > 16) {
      num+=getFormattedUSNumber(parseInt(str.substring(0,16)));
      str = str.substring(16, str.length);
    }
    return num + getFormattedUSNumber(parseInt(str));
  } else {
    return new Intl.NumberFormat('en-US', {
      maximumFractionDigits: 0,
      minimumFractionDigits: 0
    }).format(str);
  }
};
/**
 * @description This function returns formatted follower label as per design
 *              from followerCountOptions define inside const file,
 *              based on follower min and max count.
 *              If min and max not present then returns empty string.
 * @param follower object contains min and max follower count
 * @returns formatted follower count label eg. "Less than 1k"
 */