export const HTTP_RESPONSE = Object.freeze({
  OK: { code: 200, statusText: 'Ok' },
  CREATED: { code: 201, statusText: 'Created' },
  UPDATED: { code: 204, statusText: 'Updated' },
  NOT_FOUND: { code: 404, statusText: 'Not Found' },
  BAD_REQUEST: { code: 400, statusText: 'Bad Request' },
  NOT_AUTHORIZED: { code: 401, statusText: 'Not Authorized' },
  CONFLICTS: { code: 409, statusText: 'Conflict' },
  INTERNAL_SERVER_ERROR: { code: 500, statusText: 'Internal Server Error' },
  FAILED_DEPENDENCY: { code: 424, statusText: 'Failed Dependency' },
  GONE: { code: 410, statusText: 'Gone' }
});