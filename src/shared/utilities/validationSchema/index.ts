import * as Yup from 'yup';
import { FieldType } from '../../../shared/utilities/interfaces';

export const getValidation = (
  fields: FieldType[]
) => {
  const signInSchema = {};
  if (fields.length > 0) {
    fields.forEach(field => {
      if (field.validation) {
        if (field.validation.type === 'regex') {
          signInSchema[field.field] = Yup.string()
            .strict(true)
            .strict()
            .trim(`${field.label} should not include trailing whitespaces`)
            .max(field.validation?.length?.max || 0,
              field.validation?.length?.max
                ? `${field.label} should be ${field.validation.length.max} chars atmost`
                : '')
            .min(field.validation?.length?.min || 0,
              field.validation?.length?.min
                ? `${field.label} should be minimum ${field.validation.length.min} chars atleast`
                : '')
            .matches(
              field.validation.regex,
              `${field.label} ${field.validation.regexMessage}`
            )
            .required(`This field is required`)
            .nullable();
        } else if (field.validation.type === 'url') {
          if(field.validation.required) {
            signInSchema[field.field] = Yup.string()
            .strict(true)
              .trim(`${field.label} should not include trailing whitespaces`)
            .url(`Invalid URL`)
            .required(`This field is required`)
            .nullable();
          } else {
            signInSchema[field.field] = Yup.string()
            .strict(true)
              .trim(`${field.label} should not include trailing whitespaces`)
            .url(`Invalid URL`)
            .nullable();
          }         
        } else if (field.validation.type === 'email') {
          signInSchema[field.field] = Yup.string()
            .strict(true)
              .trim(`${field.label} should not include trailing whitespaces`)
            .email('You have entered incorrect email address')
            .required(`This field is required`)
            .nullable();
        } else {
          if(field.validation.required) {
            signInSchema[field.field] = Yup.string()
            .strict(true)
              .trim(`${field.label} should not include trailing whitespaces`)
            .max(field.validation?.length?.max || 0,
              field.validation?.length?.max
                ? `${field.label} should be maximum ${field.validation.length.max} chars`
                : '')
            .min(field.validation?.length?.min || 0,
              field.validation?.length?.min
                ? `${field.label} should be minimum ${field.validation.length.min} chars`
                : '')
            .required(`This field is required`)
            .nullable();
          } else {
            signInSchema[field.field] = Yup.string()
              .strict(true)
              .trim(`${field.label} should not include trailing whitespaces`)
              .max(field.validation?.length?.max || 0,
                field.validation?.length?.max
                  ? `${field.label} should be maximum ${field.validation.length.max} chars`
                  : '')
              .min(field.validation?.length?.min || 0,
                field.validation?.length?.min
                  ? `${field.label} should be minimum ${field.validation.length.min} chars`
                  : '')
              .nullable();
          } 
        }
      }
    });
  }

  return Yup.object().shape(signInSchema);
};
