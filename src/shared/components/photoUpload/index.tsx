import React, { useState } from 'react';
import { Button, Dialog, FormHelperText, Paper } from '@mui/material';
import ImageIcon from '@mui/icons-material/Image';
import { Box, styled } from '@mui/system';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import { enableModal } from 'core/store/app/actions';
import { connect } from 'react-redux';
import { IModalData } from 'shared/utilities/interfaces';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

const PhotoUploadComponent = styled(Box)(({ theme }) => ({
  justifyContent: 'center',
  flexDirection: 'column',
  display: 'flex',
  width: '200px',
  height: '235px',
  padding: '10px',
  boxSizing: 'border-box',
  borderRadius: '4px',
  border: `1px solid ${theme.palette.claraPalette.gray3}`,
  '.dotted-container': {
    width: '180px',
    height: '180px',
    boxSizing: 'border-box',
    borderRadius: '4px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: '10px',
    backgroundColor: theme.palette.claraPalette.gray4,
  },
  '.img-container': {
    width: '180px',
    height: '180px',
    boxSizing: 'border-box',
    borderRadius: '4px',
  },
}));

const imageFileTypeSupported = ['jpg', 'jpeg', 'png', 'bmp', 'gif'];

interface IPhotoUpload {
  userId?: string;
  // eslint-disable-next-line no-unused-vars
  uploadFile?: (userId: string, formData: any) => void;
  defaultImage?: string;
  buttonName?: string;
  helpertext?: boolean;
  // eslint-disable-next-line no-unused-vars
  enableAppModal: (data: IModalData) => void;
  forwardedRef?;
}

const PhotoUpload = (photoUploadProps: IPhotoUpload) => {

  const {
    userId,
    uploadFile,
    defaultImage,
    buttonName,
    helpertext,
    forwardedRef
  } = photoUploadProps;

  const [img, setImg] = useState<File>();
  const [fileSizeError, setFileSizeError] = useState<boolean>(false);
  const [fileTypeError, setFileTypeError] = useState<boolean>(false);

  const [isError, setIsError] = useState<boolean>(false);
  const [cropperModel, setCropperModel] = useState<boolean>(false);
  const [src, setSrc] = useState<any>();
  const [newCropImage, setNewCropImage] = useState<any>();
  const [crop, setCrop] = useState<any>({
    unit: '%',
    width: 30,
    aspect: 1 / 1
  });
  const [finalCropImage, setFinalCropImage] = useState<any>();
  const [fileInfo, setFileInfo] = useState<any>({
    name: '',
    type: ''
  });

  const [preview, setPreview] = useState<string>();

  const uploadImage = () => {
    if (finalCropImage) {
      const size = parseInt(process.env.REACT_APP_MAX_FILE_SIZE) * 1024;

      const imageFile = new File([finalCropImage], finalCropImage.name);
      if ((imageFile.size / 1024) > size) {
        setImg(null);
        setFileSizeError(true);
        setIsError(true);
        setCropperModel(false);
        return;
      }
      const reader = new FileReader();
      const formData = new FormData();
      formData.append('image', imageFile);
      reader.onload = () => {
        const image = new Image();
        image.src = reader.result as string;
        image.onload = () => {
          uploadFile(userId, formData);
          setPreview(reader.result as string);
          setCropperModel(false);
        };
      };
      reader.readAsDataURL(finalCropImage);
    } else {
      if (!preview) {
        setPreview(null);
      }
    }
  };

  const handleChange = (e) => {
    const [file] = e.target.files;
    if (file) {
      // File should be in jpg|jpeg|png|bmp|gif
      if (imageFileTypeSupported.includes(file.type.split("/")[file.type.split.length - 1])) {
        setFileTypeError(false);
        setImg(file);
        setCropperModel(true);
        setFileInfo({
          name: file.name,
          type: file.type
        });
        const reader: any = new FileReader();
        reader.addEventListener('load', () =>
          setSrc(reader.result)
        );
        reader.readAsDataURL(file);
      } else {
        setFileSizeError(false);
        setFileTypeError(true);
        setImg(null);
        setIsError(true);
        return;
      }
    } else {
      setImg(null);
    }
  };

  function handleClick(e) {
    setIsError(false);
    setFileSizeError(false);
    setFileTypeError(false);
    setSrc(null);
    e.target.value = null;
  }

  const onCropChange = (crop) => {
    // You could also use percentCrop:
    setCrop(crop);
  };

  const onImageLoaded = (image) => {
    setNewCropImage(image);
  };

  const onCropComplete = (crop) => {
    makeClientCrop(crop);
  };

  const closeModel = () => {
    setCropperModel(false);
    setIsError(false);
    setSrc(null);
  };

  async function makeClientCrop(crop) {
    if (newCropImage && crop.width && crop.height) {
      const croppedImage = await getCroppedImg(
        newCropImage,
        crop,
        fileInfo.name
      );
      setFinalCropImage(croppedImage);
    }
  }

  function getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement('canvas');
    const pixelRatio = window.devicePixelRatio;
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext('2d');

    canvas.width = crop.width * pixelRatio * scaleX;
    canvas.height = crop.height * pixelRatio * scaleY;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = 'high';

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width * scaleX,
      crop.height * scaleY
    );

    return new Promise((resolve) => {
      canvas.toBlob(
        (blob: any) => {
          if (!blob) {
            // reject(new Error('Canvas is empty'));
            return;
          }
          blob.name = fileName;
          resolve(blob);
        },
        fileInfo.type,
        1
      );
    });
  }

  return (
    <>
      <PhotoUploadComponent>
        <Box className="dotted-container">
          {preview ? (
            <img src={preview} className="img-container" />
          ) : defaultImage ? (
            <img src={defaultImage} className="img-container" />
          ) : (
            <ImageIcon color="primary" fontSize="large" />
          )}
        </Box>
        <Button
          variant="outlined"
          style={{
            fontSize: "12px",
            width: "180px",
            height: "25px",
            borderRadius: "12px",
          }}
          component="label"
        >
          {buttonName || "Browse Image"}
          <input
            type="file"
            name="image-upload"
            hidden
            ref={forwardedRef}
            accept="image/x-png,image/gif,image/jpeg,image/bmp"
            onChange={handleChange}
            onClick={handleClick}
          />
        </Button>
      </PhotoUploadComponent>
      <FormHelperText
        id="phote-error"
        sx={
          fileSizeError || fileTypeError || helpertext ?
            { color: "#EB5757", textAlign: "center", maxWidth: '200px', whiteSpace: 'nowrap' }
            :
            { color: 'rgba(0,0,0,0.38)', textAlign: "center", maxWidth: '200px', whiteSpace: 'nowrap' }
        }
      >
        {
          helpertext && !preview && !fileSizeError && !fileTypeError ? "This field is required" :
            fileTypeError && !fileSizeError ? "Only image files are allowed !" :
              fileSizeError ? "Image size should be less than " + process.env.REACT_APP_MAX_FILE_SIZE_TEXT :
                "(Upload square image upto 5 MB size)"
        }
      </FormHelperText>
      {
        (img && !isError && cropperModel) ?
          <BootstrapDialog
            onClose={closeModel}
            aria-labelledby="customized-dialog-title"
            open={cropperModel}
            sx={{
              background: 'rgba(26, 26, 26, 0.5)'
            }}
          >
            <Paper
              sx={{
                position: "fixed",
                top: "50%",
                left: "50%",
                transform: "translate(-50%, -50%)",
                width: "500px",
                height: "500px",
                // maxHeight: "500px",
                background: "#FFFFFF",
                boxShadow:
                  "0px 7px 8px -4px rgba(0, 0, 0, 0.2), 0px 12px 17px 2px rgba(0, 0, 0, 0.14), 0px 5px 22px 4px rgba(0, 0, 0, 0.12)",
                borderRadius: "12px",
                border: 'none',
                zIndex: '1',
                padding: '25px'
              }}
            >
              <Box
                sx={{
                  width: '500px',
                  height: '450px',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <ReactCrop
                  src={src}
                  crop={crop}
                  ruleOfThirds
                  onImageLoaded={onImageLoaded}
                  onComplete={onCropComplete}
                  onChange={onCropChange}
                  style={{
                    maxWidth: '450px',
                    maxHeight: '450px',
                  }}
                />
              </Box>
              <Box
                sx={{
                  width: '500px',
                  position: 'fixed',
                  bottom: '20px',
                  display: 'flex',
                  justifyContent: 'space-around',
                  alignItems: 'center'
                }}>
                <Button
                  variant="contained" size="large"
                  sx={{
                    ":hover": {
                      bgcolor: "claraPalette.gray3", // theme.palette.primary.main
                    },
                    bgcolor: "#F2F2F2",
                    color: "claraPalette.darkGray",
                    mr: "0.875rem",
                    width: '150px'
                  }} onClick={closeModel}>
                  Cancel
                </Button>
                <Button
                  variant="contained" size="large"
                  type="submit"
                  sx={{
                    mr: "0.875rem",
                    width: '150px'
                  }} onClick={uploadImage}>
                  Submit
                </Button>
              </Box>
            </Paper>
          </BootstrapDialog>
          :
          <></>
      }
    </>
  );
};
export default connect(() => ({}), {
  enableAppModal: enableModal,
})(PhotoUpload);