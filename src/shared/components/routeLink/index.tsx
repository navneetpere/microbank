import React from 'react';
import { Box, styled } from '@mui/system';
import { Link as RouterLink } from 'react-router-dom';
import { LinkProps } from '@mui/material';

const StyledLink = styled(Box)(({ theme }) => ({
  marginTop: '1rem',
  color: theme.palette.primary.main,
  textDecoration: 'none',
  '&:hover': {
    textDecoration: 'underline'
  },
  display: 'inline',
  fontSize: '14px'
}));

interface IRouteLink extends LinkProps {
  to: string;
}

function RouteLink(RouteProps: IRouteLink) {
  const { to, children } = RouteProps;
  return (<RouterLink to={to} style={{ textDecoration: 'none' }}>
    <StyledLink {...RouteProps}>
      {children}
    </StyledLink>
  </RouterLink>);
}

export default RouteLink;