export function getSessionStoreage (key: string) {
  const result = localStorage.getItem(key);
  try {
    return JSON.parse(result === null ? '' : result);
  } catch (e) {
    return result;
  }
}

export const undefinedArray = [null, undefined, ''];

export function getFromSession (key: string): {
  success: boolean,
  result?: string
} {
  const result = localStorage.getItem(key);
  if (undefinedArray.indexOf(result) === -1) {
    return {
      success: true,
      result
    };
  }
  return {
    success: false,
  };
}

export function setSessionItem (key: string, value: string) {
  if (key !== '' && value !== '') {
    localStorage.setItem(key, value);
  }
}

export const clearSessionStorage = () => {
  localStorage.clear();
};

export const clearLocalStorage = () => {
  localStorage.clear();
};

