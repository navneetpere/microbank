import React from 'react';
import ReactDOM from 'react-dom';
import { ConnectedRouter } from 'connected-react-router';
import { Provider } from 'react-redux';

import './index.css';
import RootApp from './app';
import reportWebVitals from './reportWebVitals';
import store, { history } from './store/store';

ReactDOM.render(
	<Provider store={store}>
		<ConnectedRouter history={history}>
			<RootApp />
		</ConnectedRouter>
	</Provider>,
	document.getElementById('root')
);

reportWebVitals();
