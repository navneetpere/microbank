import React, { Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { userRoutes } from './routes';
import { IState } from '../shared/utilities/interfaces';
import AppLoader from '../core/components/appLoader';
// import { AccountType } from '../features/auth/utilites';
import NotFound from '../core/components/notFound';
import PublicRoutes from './publicRoutes';
// interface IMainRoutes {
//   isLoggedIn: boolean;
//   accountType: AccountType;
//   status: string;
// }

const Routes = () => {
    return (
      <Switch>
        {userRoutes.map((singleRoute) => {
          return (
            <Route
              component={singleRoute.component}
              exact={singleRoute.exact}
              path={singleRoute.path}
              key={singleRoute.path}
            />
          );
        })}
        <Route>
          <NotFound to={`/home`} navLinkName='Home' />
        </Route>
      </Switch>
    );
};

const MainRoutes = () => {
  return (
    <Suspense fallback={<AppLoader />}>
      <Switch>
        <Routes/>
        <PublicRoutes />
      </Switch>
    </Suspense>
  );
};

export default connect((state: IState) => ({
  isLoggedIn: state.auth.isLoggedIn,
  accountType: state.auth.accountType,
  status: state.auth.status
}))(MainRoutes);