import { lazy } from 'react';

export const openRoutes = [
  {
    path: "/auth",
    exact: false,
    component: lazy(() => import("../features/auth/container/index")),
  },
];


export const userRoutes = [
  {
    path: "/home",
    exact: false,
    component: lazy(() => import("../features/user/homePage/container/index")),
  },
  {
    path: "/profile",
    exact: false,
    component: lazy(() => import("../features/user/profile/container/index")),
  },
];
