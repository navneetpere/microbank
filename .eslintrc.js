module.exports = {
	env: {
		browser: true,
		es2021: true
	},
	extends: [
		'eslint:recommended',
		'plugin:react/recommended',
		'plugin:@typescript-eslint/eslint-recommended',
		'prettier'
	],
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaFeatures: {
			jsx: true
		},
		ecmaVersion: 2018,
		sourceType: 'module',
		project: './tsconfig.json'
	},
	settings: {
		react: {
			version: 'detect'
		},
		'import/resolver': {
			node: {
				extensions: ['.js', '.jsx', '.ts', '.tsx'],
				moduleDirectory: ['node_modules', 'src/']
			}
		}
	},
	plugins: ['react', '@typescript-eslint', 'react-hooks',"import-quotes"],
	rules: {
		"global-require": 0,
		"no-plusplus": "error",
		"global-require": 0,
		"max-len": [
		  "warn",
		  {
			ignoreTemplateLiterals: true,
			ignoreStrings: true
		  }
		],
		"import-quotes/import-quotes": [2, "single"],
		"import/no-unresolved": ["off"],
		"import/prefer-default-export": ["off"],
		"import/no-dynamic-require": ["off"],
		"import/no-cycle": ["off"],
		"prefer-promise-reject-errors": ["error"],
		"no-underscore-dangle": ["off"],
		"react/jsx-filename-extension": ["off"],
		"react/jsx-props-no-spreading": 1,
		"react/prop-types": ["error"],
		"react/jsx-no-duplicate-props": ["error", { "ignoreCase": false }],
		"react/prefer-stateless-function": ["warn"],
		"react/no-unescaped-entities": ["error"],
		 "react/destructuring-assignment": ["off"],
		"no-return-assign": ["error"],
		"no-console": 2,
		"no-unused-expressions": ["error"],
		"no-bitwise": ["error"],
		"semi": 2,
		"@typescript-eslint/semi": ["error"],
		"jsx-a11y/click-events-have-key-events": "off",
		"spaced-comment": [
		  "error",
		  "always",
		  {
			markers: ["/"]
		  }
		],
		"no-param-reassign": 0,
		"react/jsx-wrap-multilines": ["error"],
		"prefer-destructuring": ["error"],
		"import/extensions": ["off"],
		"@typescript-eslint/no-explicit-any": ["off"],  
		"react/prop-types": 0,
		"no-undef": 0,
		"prefer-destructuring": ["error"],
		"no-unused-vars": [
		  "error",
		  {
			vars: "all",
			args: "after-used",
			ignoreRestSiblings: false
		  }
		],
		"linebreak-style": ["off", "unix"],
		"react-hooks/rules-of-hooks": "error",
		"react-hooks/exhaustive-deps": "off"
	},
	ignorePatterns: [
		"src/__tests__",
		"src/types",
		"src/assets",
		"src/config",
		"src/react-app-env.d.ts",
		"src/reportWebVitals.ts",
		"src/serviceWorker.ts",
		"src/setupTests.ts",
		"src/tests",
		"src/SiteContext.tsx"
	],
}
